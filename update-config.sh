#!/bin/bash

gitlab-runner exec docker \
  --docker-volumes $(readlink -f .):/tmp/local update-config
