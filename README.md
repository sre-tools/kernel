Kernel
======

Build script(s) to a custom Linux kernel.

To upgrade a kernel simply modify the following two variables in the [.gitlab-ci.yml](.gitlab-ci.yml):
```
# directory on the server with the actual tarballs
KERNEL_BRANCH: "v5.x"
# version to fetch and build
KERNEL_VERSION: "5.4.214"
```

Additional update the [reference-config](reference-config) with a fitting configuration by importing it e.g. from an elrepo kernel or by updating it automatically via the integrated `make oldconfig` CI job - this is **not supposed to be run via CI** but **locally** via:
```
gitlab-runner exec docker --docker-volumes $(readlink -f .):/tmp/local update-config
```
Note: The version changes need to commited already. You can simply ammend the config adaption afterwards.
